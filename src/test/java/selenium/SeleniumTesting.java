package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.util.ArrayList;

public class SeleniumTesting {
    private static final String DRIVER_PATH = "./webdriver/chromedriver";
    private static WebDriver driver;
    private static Actions actions;

    public static void main(String[] args) throws InterruptedException, AWTException {
        initializeChromeDriver();
        driver.get("https://www.flipkart.com");
        Thread.sleep(1000);
        actions = new Actions(driver);
        moveToElectronics();
        Thread.sleep(1000);
        orderItem();
        moveToNextTab();
        Thread.sleep(2000);
        proceedToCheckout();
        Thread.sleep(2500);
        endSession();
    }

    public static void initializeChromeDriver() {
        System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    public static void moveToElectronics() throws InterruptedException {
        driver.findElement(By.xpath("//button[@class='_2KpZ6l _2doB4z']")).click();
        actions.moveToElement(driver.findElement(By.linkText("Electronics"))).perform();
        Thread.sleep(1000);
        driver.findElement(By.linkText("Gaming")).click();
    }

    public static void orderItem() {
        driver.findElement(By.xpath("//input[@name='q']")).sendKeys("ASUS TUF Gaming");
        driver.findElement(By.xpath("//button[@class='L0Z3Pu']")).click();
        try {
            Thread.sleep(2000);
            WebElement imageElement = driver.findElement(By.xpath("//img[@class='_396cs4 _3exPp9']"));
            actions.moveToElement(imageElement).click().perform();
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
    }

    public static void moveToNextTab() {
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabs.size() - 1));
    }

    public static void proceedToCheckout() throws InterruptedException {
        driver.findElement(By.xpath("//button[@class='_2KpZ6l _2U9uOA _3v1-ww']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//button[@class='_2KpZ6l _2ObVJD _3AWRsL']")).click();
        Thread.sleep(1000);
    }

    public static void endSession() {
        driver.close();
        driver.quit();
    }
}